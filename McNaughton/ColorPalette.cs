﻿using System.Collections.Generic;
using System.Windows.Media;
using McNaughton.Model;

namespace McNaughton
{
    public static class ColorPalette
    {
        public static Color Find(Id forId)
        {
            var offset = (int)forId.Value % _palette.Count;

            return Color.FromRgb(
                _palette[offset][0],
                _palette[offset][1],
                _palette[offset][2]);
        }

        private static readonly List<byte[]> _palette
            = new List<byte[]>
            {
                new byte[] { 240, 163, 255 },
                new byte[] { 0, 117, 220 },
                new byte[] { 153, 63, 0 },
                new byte[] { 76, 0, 92 },
                new byte[] { 25, 25, 25 },
                new byte[] { 0, 92, 49 },
                new byte[] { 43, 206, 72 },
                new byte[] { 255, 204, 153 },
                new byte[] { 128, 128, 128 },
                new byte[] { 148, 255, 181 },
                new byte[] { 143, 124, 0 },
                new byte[] { 157, 204, 0 },
                new byte[] { 194, 0, 136 },
                new byte[] { 0, 51, 128 },
                new byte[] { 255, 164, 5 },
                new byte[] { 255, 168, 187 },
                new byte[] { 66, 102, 0 },
                new byte[] { 255, 0, 16 },
                new byte[] { 94, 241, 242 },
                new byte[] { 0, 153, 143 },
                new byte[] { 224, 255, 102 },
                new byte[] { 116, 10, 255 },
                new byte[] { 153, 0, 0 },
                new byte[] { 255, 255, 128 },
                new byte[] { 255, 255, 0 },
                new byte[] { 255, 80, 5}
            };
    }
}
