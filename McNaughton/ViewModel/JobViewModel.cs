﻿using System.ComponentModel;
using McNaughton.Model;

namespace McNaughton.ViewModel
{
    internal sealed class JobViewModel : INotifyPropertyChanged
    {
        private string _description;
        private uint _processingTime;

        public event PropertyChangedEventHandler PropertyChanged;

        public Id Id { get; }
        public string Name { get; }

        public string Description
        {
            get => _description;
            set
            {
                _description = value;
                NotifyChanged(nameof(Description));
            }
        }

        public uint ProcessingTime
        {
            get => _processingTime;
            set
            {
                _processingTime = value;
                NotifyChanged(nameof(ProcessingTime));
            }
        }

        public JobViewModel(IJob model)
        {
            Id = model.Id;
            Name = model.Name;

            _description = model.Description;
            _processingTime = model.ProcessingTime;
        }

        public JobViewModel(Id id)
        {
            Id = id;
            Name = $"J{id}";

            _description = "";
            _processingTime = 1;
        }

        public IJob ToModel()
            => new Job(Id, _description, _processingTime);

        private void NotifyChanged(string propertyName)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}
