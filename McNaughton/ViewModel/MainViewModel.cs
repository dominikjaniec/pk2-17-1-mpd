﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Data;
using McNaughton.JobSource;
using McNaughton.Model;
using McNaughton.Model.Schedulers;

namespace McNaughton.ViewModel
{
    internal sealed class MainViewModel : INotifyPropertyChanged
    {
        private uint _messageLogCount = 0;
        private uint _machinesCount = 1;
        private string _sourceFilePath = "";
        private ISource _sourceLoader = null;

        public event PropertyChangedEventHandler PropertyChanged;

        public string LogMessage { get; private set; }

        public uint MachinesCount
        {
            get => _machinesCount;
            set
            {
                _machinesCount = value;
                NotifyChanged(nameof(MachinesCount));
                NotifyChanged(nameof(Schedulable));
            }
        }

        public string SourceFilePath
        {
            get => _sourceFilePath;
            set
            {
                _sourceFilePath = value;
                _sourceLoader = SourceLoaders.Resolve(value);
                NotifyChanged(nameof(SourceFilePath));
                NotifyChanged(nameof(KnownSourceType));

                if (!KnownSourceType)
                    Log($"Nieobsługiwany typ pliku: {value}");
            }
        }

        public bool KnownSourceType
            => _sourceLoader != null;

        public bool Schedulable
            => JobsCollection.Any()
                && _machinesCount > 0;

        public ICollectionView DefinedJobs { get; set; }

        public void ReloadJobs()
        {
            try
            {
                var jobs = _sourceLoader.Jobs()
                    .Select(job => new JobViewModel(job))
                    .ToList();

                DefinedJobs = CollectionViewSource.GetDefaultView(jobs);
                NotifyChanged(nameof(DefinedJobs));
                NotifyChanged(nameof(Schedulable));

                Log($"Załadowano {jobs.Count} zadań z pliku: '{_sourceFilePath}'.");
            }
            catch (Exception ex)
            {
                Log(ex.Message);
            }
        }

        public void AddNewJobItem()
        {
            var id = FindNextFreeJobId();
            JobsCollection.Add(new JobViewModel(id));

            DefinedJobs.Refresh();
            NotifyChanged(nameof(DefinedJobs));
            NotifyChanged(nameof(Schedulable));
        }

        public void ReScheduleJobsAndDrawOn(Canvas canvas)
        {
            if (!Schedulable)
            {
                Log("Niemożna uszeregować gdy nie ma zadań lub maszyn.");
                return;
            }

            var totalWatch = Stopwatch.StartNew();
            var jobs = JobsCollection
                .Select(jvm => jvm.ToModel())
                .ToList();

            var machines = Algorithms.McNaughton
                .Schedule(_machinesCount, jobs)
                .ToList();

            var renderLog = RenderSchedule(canvas, jobs, machines);

            totalWatch.Stop();
            Log($"Uszeregowano {jobs.Count} zadań na {_machinesCount} maszynach w czasie: {totalWatch.ElapsedMilliseconds} ms ({renderLog}).");
        }

        private IList<JobViewModel> JobsCollection
        {
            get
            {
                if (DefinedJobs == null)
                    DefinedJobs = CollectionViewSource
                        .GetDefaultView(new List<JobViewModel>());

                return (IList<JobViewModel>)DefinedJobs?.SourceCollection;
            }
        }

        private Id FindNextFreeJobId()
        {
            var lastId = JobsCollection.Select(jvm => jvm.Id)
                .Concat(new[] { Id.Undefined })
                .Max();

            return lastId.IsUndefined
                ? Id.From(1)
                : lastId.Next();
        }

        private void Log(string message)
        {
            LogMessage = $"{++_messageLogCount}. {message}";
            NotifyChanged(nameof(LogMessage));
        }

        private void NotifyChanged(string propertyName)
            => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        private static string RenderSchedule(Canvas canvas, IReadOnlyList<IJob> jobs, IReadOnlyList<IMachine> machines)
        {
            var log = "Render: ";
            try
            {
                var renderWatch = Stopwatch.StartNew();
                ScheduleDrawer.RenderOn(canvas, jobs, machines);

                renderWatch.Stop();
                log += renderWatch.ElapsedMilliseconds + " ms";
            }
            catch (Exception ex)
            {
                log += ex.Message;
            }

            return log;
        }
    }
}
