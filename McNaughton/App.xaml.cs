﻿using System.Diagnostics;
using System.IO;
using System.Windows;

namespace McNaughton
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static string InitialSourcePath = string.Empty;
        public static uint InitialMachineCount = 0;

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            var current = Directory.GetCurrentDirectory();
            Log($"Current directory: '{current}'");

            if (e.Args.Length > 0)
            {
                InitialSourcePath = Path.Combine(current, e.Args[0]);
                Log($"Provided source file: '{InitialSourcePath}'");
            }

            if (e.Args.Length > 1)
            {
                InitialMachineCount = uint.Parse(e.Args[1]);
                Log($"Provided machines count: {InitialMachineCount}");
            }
        }

        private static void Log(string message)
            => Debug.WriteLine($"#### {message}");
    }
}
