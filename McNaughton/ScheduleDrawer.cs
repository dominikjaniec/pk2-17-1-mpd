﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using McNaughton.Model;

namespace McNaughton
{
    internal static class ScheduleDrawer
    {
        public static void RenderOn(Canvas canvas, IReadOnlyList<IJob> jobs, IReadOnlyList<IMachine> machines)
        {
            var ctx = new Context(canvas, jobs, machines);

            ctx.PreapreCanvas();
            DrawTimeLegend(ctx, canvas.ActualHeight);

            uint machineRow = 0;
            foreach (var machine in ctx.Machines.OrderBy(m => m.Id))
                DrawMachineSchedule(ctx, ++machineRow, machine);
        }

        private static void DrawTimeLegend(Context ctx, double indicatorHeight)
        {
            var indicatorBrush = new SolidColorBrush(Colors.LightBlue);
            var indicatorDash = new DoubleCollection { 4.0, 2.0 };

            void DrawSliceIndicator(uint t)
            {
                var x = (t + 1) * ctx.UnitDim;
                var line = new Line
                {
                    Stroke = indicatorBrush,
                    StrokeDashArray = indicatorDash,
                    StrokeThickness = 1.0,
                    X1 = x,
                    Y1 = 0.0,
                    X2 = x,
                    Y2 = indicatorHeight
                };

                ctx.DrawIt(line);
            }

            var timeLabelBrush = new SolidColorBrush(Colors.DarkSlateBlue);

            for (uint t = 0; t <= ctx.MaxTime; ++t)
            {
                DrawSliceIndicator(t);
                DrawLabel(ctx, 0, t + 1, txt =>
                {
                    txt.Text = $"t{t}";
                    txt.FontSize = ctx.UnitDim / 3.0;
                    txt.Foreground = timeLabelBrush;
                });
            }
        }

        private static void DrawMachineSchedule(Context ctx, uint row, IMachine machine)
        {
            DrawLabel(ctx, row, 0, txt =>
            {
                txt.Text = machine.Name;
                txt.ToolTip = machine.Description;
                txt.FontSize = ctx.UnitDim / 2.0;
                txt.FontWeight = FontWeights.Bold;
                txt.Foreground = new SolidColorBrush(Colors.Black);
            });

            DrawMachineTicks(ctx, row, machine.Ticks);
        }

        private static void DrawMachineTicks(Context ctx, uint row, ITicksSchedule schedule)
        {
            var jobLabelBrush = new SolidColorBrush(Colors.WhiteSmoke);

            void DrawJobTick(ITick tick, uint col)
            {
                DrawBlock(ctx, row, col, rec =>
                {
                    rec.Height = ctx.UnitDim * 0.7;
                    rec.Width = ctx.UnitDim * 0.9;
                    rec.Fill = ctx.BrushFor(tick.Occupier);
                });

                DrawLabel(ctx, row, col, txt =>
                {
                    txt.Text = tick.Occupier.Name;
                    txt.FontSize = ctx.UnitDim / 3.0;
                    txt.Foreground = jobLabelBrush;
                });
            }

            for (uint t = 0; t <= schedule.LastTime; ++t)
            {
                var jobTick = schedule[t];
                if (!jobTick.HasJob)
                    continue;

                DrawJobTick(jobTick, t + 1);
            }
        }

        private static void DrawLabel(Context ctx, uint row, uint col, Action<TextBlock> setter = null)
        {
            var label = new TextBlock
            {
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center
            };

            setter?.Invoke(label);
            ctx.DrawIt(row, col, label);
        }

        private static void DrawBlock(Context ctx, uint row, uint col, Action<Rectangle> setter = null)
        {
            var rectange = new Rectangle
            {
                HorizontalAlignment = HorizontalAlignment.Center,
                VerticalAlignment = VerticalAlignment.Center
            };

            setter?.Invoke(rectange);
            ctx.DrawIt(row, col, rectange);
        }

        private class Context
        {
            private readonly uint _maxJobIdValue;
            private readonly Canvas _canvas;

            public IReadOnlyList<IJob> Jobs { get; }
            public IReadOnlyList<IMachine> Machines { get; }

            public uint MaxTime { get; }
            public uint UnitDim { get; }

            public Context(Canvas canvas, IReadOnlyList<IJob> jobs, IReadOnlyList<IMachine> machines)
            {
                Jobs = jobs;
                Machines = machines;
                MaxTime = FindMaxTime(Machines);
                UnitDim = FindUnitDim(canvas, Machines);

                _canvas = canvas;
                _maxJobIdValue = FindMaxJobId(Jobs);
            }

            public void PreapreCanvas()
            {
                _canvas.Children.Clear();
                _canvas.Width = (MaxTime + 2) * UnitDim;
                _canvas.VerticalAlignment = VerticalAlignment.Stretch;
            }

            public void DrawIt(UIElement element)
                => _canvas.Children.Add(element);

            public void DrawIt(uint row, uint col, UIElement element)
            {
                var box = new Border
                {
                    BorderBrush = null,
                    Child = element,
                    Height = UnitDim,
                    Width = UnitDim,
                };

                Canvas.SetLeft(box, col * UnitDim);
                Canvas.SetBottom(box, row * UnitDim);

                DrawIt(box);
            }

            public Brush BrushFor(IJob job)
                => new SolidColorBrush(ColorPalette.Find(job.Id));

            private static uint FindMaxJobId(IEnumerable<IJob> jobs)
                => jobs
                    .Select(j => j.Id.Value)
                    .Concat(new uint[] { 1 })
                    .Max();

            private static uint FindMaxTime(IEnumerable<IMachine> machines)
                => machines
                    .Select(m => m.Ticks.LastTime)
                    .Concat(new uint[] { 0 })
                    .Max();

            private static uint FindUnitDim(FrameworkElement canvas, IReadOnlyCollection<IMachine> machines)
            {
                const double maxDim = 69.42;
                var unit = canvas.ActualHeight / (machines.Count + 1);
                return (uint)(unit > maxDim ? maxDim : unit);
            }
        }
    }
}
