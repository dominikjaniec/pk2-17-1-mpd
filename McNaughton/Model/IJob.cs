﻿namespace McNaughton.Model
{
    public interface IJob : IItem
    {
        uint ProcessingTime { get; }
    }
}
