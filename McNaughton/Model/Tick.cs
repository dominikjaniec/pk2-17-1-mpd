﻿namespace McNaughton.Model
{
    internal class Tick : ITick
    {
        public uint Time { get; }
        public IJob Occupier { get; }

        public bool HasJob
            => Occupier != null;

        public static ITick Empty(uint time)
            => new Tick(time, occupier: null);

        public static ITick With(IJob job, uint time)
            => new Tick(time, occupier: job);

        private Tick(uint time, IJob occupier)
        {
            Time = time;
            Occupier = occupier;
        }
    }
}
