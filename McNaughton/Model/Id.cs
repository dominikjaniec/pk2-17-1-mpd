﻿using System;
using System.Globalization;

namespace McNaughton.Model
{
    public struct Id : IEquatable<Id>, IComparable<Id>
    {
        public static readonly Id Undefined = new Id(0);

        public uint Value { get; }

        public bool IsUndefined
            => Value == 0;

        public Id Next()
            => Id.From(Value + 1);

        public int CompareTo(Id other)
            => Value.CompareTo(other.Value);

        public bool Equals(Id other)
            => Value.Equals(other.Value);

        public override string ToString()
            => IsUndefined
                ? "<undefined>"
                : Value.ToString(CultureInfo.InvariantCulture);

        public override int GetHashCode()
            => Value.GetHashCode();

        public void ThrowOnUndefinedAs(string paramName)
        {
            if (IsUndefined)
                throw new ArgumentException("Undefined Id is not usable here.", paramName);
        }

        public static Id From(int id)
            => id <= 0
                ? throw new ArgumentException("Defined Id should be a number greater then 0.", nameof(id))
                : new Id((uint)id);

        public static Id From(uint id)
            => From((int)id);

        private Id(uint id)
            => Value = id;
    }
}
