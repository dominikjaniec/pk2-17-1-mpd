﻿namespace McNaughton.Model
{
    public interface IItem
    {
        Id Id { get; }
        string Name { get; }
        string Description { get; }
    }
}
