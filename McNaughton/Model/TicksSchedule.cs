﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace McNaughton.Model
{
    internal class TicksSchedule : ITicksSchedule
    {
        private readonly IReadOnlyDictionary<uint, IJob> _jobsByTime;
        private readonly IReadOnlyDictionary<IJob, uint[]> _timesByJob;

        public bool Loops { get; }
        public uint LastTime { get; }

        public ITick this[uint time]
            => GetTickFor(Corrected(time));

        public IEnumerable<ITick> this[IJob job]
            => GetTicksOf(EnsureNotNull(job));

        public TicksSchedule(IReadOnlyDictionary<uint, IJob> times, bool loops)
        {
            Loops = loops;
            LastTime = times.Keys.Max();

            _jobsByTime = times;
            _timesByJob = GroupByJobs(times);
        }

        public static ITicksSchedule Empty
            => new EmptySchedule();

        private static IReadOnlyDictionary<IJob, uint[]> GroupByJobs(IReadOnlyDictionary<uint, IJob> jobsByTime)
            => jobsByTime
                .Select(pair => new
                {
                    Job = pair.Value,
                    Time = pair.Key
                })
                .GroupBy(data => data.Job)
                .ToDictionary(
                    gr => gr.Key,
                    gr => gr
                        .Select(data => data.Time)
                        .ToArray());

        private static IJob EnsureNotNull(IJob job)
            => job ?? throw new ArgumentNullException(nameof(job));

        private uint Corrected(uint time)
            => time > LastTime && Loops
                ? time % (LastTime + 1)
                : time;

        private ITick GetTickFor(uint time)
            => _jobsByTime.TryGetValue(time, out var job)
                ? Tick.With(job, time)
                : Tick.Empty(time);

        private IEnumerable<ITick> GetTicksOf(IJob job)
            => _timesByJob.TryGetValue(job, out var times)
                ? times.Select(t => Tick.With(job, t))
                : Enumerable.Empty<ITick>();

        private class EmptySchedule : ITicksSchedule
        {
            public bool Loops { get; } = true;
            public uint LastTime { get; } = 0;

            public ITick this[uint time]
                => Tick.Empty(time);

            public IEnumerable<ITick> this[IJob job]
                => Enumerable.Empty<ITick>();
        }
    }
}
