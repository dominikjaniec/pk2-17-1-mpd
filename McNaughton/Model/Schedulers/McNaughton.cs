﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace McNaughton.Model.Schedulers
{
    internal class McNaughton : IScheduler
    {
        public IEnumerable<IMachine> Schedule(uint overMachinesCount, IEnumerable<IJob> jobs)
            => ScheduleOver(overMachinesCount, jobs.ToArray())
                .Select(pair => new Machine(pair.Key, pair.Value))
                .OrderBy(machine => machine.Id);

        private static Dictionary<Id, ITicksSchedule> ScheduleOver(uint machinesCount, IJob[] jobs)
        {
            var makespan = FindMakespan(machinesCount, jobs);

            var scheduleByMachine = new Dictionary<Id, ITicksSchedule>();
            var schedule = new Dictionary<uint, IJob>(capacity: (int)makespan);
            var currentId = Id.From(1);
            uint totalTicks = 0;

            foreach (var job in Randomized(jobs))
            {
                var jobTicks = 0;
                while (jobTicks < job.ProcessingTime)
                {
                    if (totalTicks >= makespan)
                    {
                        scheduleByMachine.Add(currentId, new TicksSchedule(schedule, loops: false));
                        schedule = new Dictionary<uint, IJob>(capacity: (int)makespan);
                        currentId = currentId.Next();
                        totalTicks = 0;
                    }

                    schedule.Add(totalTicks, job);
                    ++totalTicks;
                    ++jobTicks;
                }
            }

            scheduleByMachine.Add(currentId, new TicksSchedule(schedule, loops: false));
            return FillRestWithEmptySchedule(scheduleByMachine, currentId, machinesCount);
        }

        private static uint FindMakespan(uint machineCount, IJob[] jobs)
        {
            var processingTimes = jobs.Select(j => j.ProcessingTime).ToArray();
            var totalTime = processingTimes.Select(pi => (int)pi).Sum();

            var cStartMax = Math.Max(
                totalTime / (double)machineCount,
                processingTimes.Max());

            return (uint)Math.Ceiling(cStartMax);
        }

        private static IEnumerable<IJob> Randomized(IJob[] jobs)
        {
            var rand = new Random();
            return jobs
                .OrderBy(_ => rand.Next());
        }

        private static Dictionary<Id, ITicksSchedule> FillRestWithEmptySchedule(Dictionary<Id, ITicksSchedule> scheduleByMachine, Id currentId, uint count)
        {
            var empty = TicksSchedule.Empty;
            currentId = currentId.Next();
            while (currentId.Value <= count)
            {
                scheduleByMachine.Add(currentId, empty);
                currentId = currentId.Next();
            }

            return scheduleByMachine;
        }
    }
}
