﻿using System.Collections.Generic;

namespace McNaughton.Model.Schedulers
{
    public interface IScheduler
    {
        IEnumerable<IMachine> Schedule(uint overMachinesCount, IEnumerable<IJob> jobs);
    }
}
