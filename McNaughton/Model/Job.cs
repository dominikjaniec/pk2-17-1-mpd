﻿namespace McNaughton.Model
{
    internal class Job : IJob
    {
        public Id Id { get; }
        public string Name { get; }
        public string Description { get; }

        public uint ProcessingTime { get; }

        public Job(Id id, string description, uint processingTime)
        {
            id.ThrowOnUndefinedAs(nameof(id));

            Id = id;
            Name = $"J{id}";
            Description = description;

            ProcessingTime = processingTime;
        }

        public override string ToString()
            => $"{Name}: p={ProcessingTime}";
    }
}
