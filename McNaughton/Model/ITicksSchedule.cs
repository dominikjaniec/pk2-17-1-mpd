﻿using System.Collections.Generic;

namespace McNaughton.Model
{
    public interface ITicksSchedule
    {
        bool Loops { get; }
        uint LastTime { get; }
        ITick this[uint time] { get; }
        IEnumerable<ITick> this[IJob job] { get; }
    }
}
