﻿namespace McNaughton.Model
{
    internal class Machine : IMachine
    {
        public Id Id { get; }
        public string Name { get; }
        public string Description { get; }

        public ITicksSchedule Ticks { get; }

        public Machine(Id id, ITicksSchedule ticks)
        {
            id.ThrowOnUndefinedAs(nameof(id));

            Id = id;
            Name = $"M{id}";
            Description = $"Machine no. {id}";

            Ticks = ticks;
        }
    }
}
