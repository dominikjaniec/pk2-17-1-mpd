﻿namespace McNaughton.Model
{
    public interface ITick
    {
        bool HasJob { get; }

        uint Time { get; }
        IJob Occupier { get; }
    }
}
