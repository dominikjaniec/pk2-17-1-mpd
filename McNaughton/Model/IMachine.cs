﻿namespace McNaughton.Model
{
    public interface IMachine : IItem
    {
        ITicksSchedule Ticks { get; }
    }
}
