﻿using System;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using McNaughton.JobSource;
using McNaughton.ViewModel;
using Microsoft.Win32;

namespace McNaughton
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly MainViewModel _vm;
        private bool _activatedFirestTime;

        public MainWindow()
        {
            InitializeComponent();

            _vm = new MainViewModel();
            DataContext = _vm;
        }

        protected override void OnActivated(EventArgs e)
        {
            base.OnActivated(e);

            if (_activatedFirestTime)
                return;

            if (App.InitialMachineCount > 0)
                _vm.MachinesCount = App.InitialMachineCount;

            if (File.Exists(App.InitialSourcePath))
            {
                _vm.SourceFilePath = App.InitialSourcePath;
                _vm.ReloadJobs();
            }

            _activatedFirestTime = true;
        }

        private void SelectSourceFileClickHandler(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                FileName = _vm.SourceFilePath,
                InitialDirectory = Directory.GetCurrentDirectory(),
                Filter = string.Join("|",
                    SourceLoaders.ValidExtensions
                        .Concat(new[] { ".*" })
                        .Select(ext => $"Pliki typu {ext}|*{ext}"))
            };

            if (openFileDialog.ShowDialog() == true)
                _vm.SourceFilePath = openFileDialog.FileName;
        }

        private void LoadJobsClickHandler(object sender, RoutedEventArgs e)
        {
            JobsGrid.CancelEdit(DataGridEditingUnit.Row);
            _vm.ReloadJobs();
        }

        private void AddNewJobItemClickHandler(object sender, RoutedEventArgs e)
        {
            JobsGrid.CommitEdit(DataGridEditingUnit.Row, exitEditingMode: true);
            _vm.AddNewJobItem();
        }

        private void ReScheduleClickHandler(object sender, RoutedEventArgs e)
        {
            JobsGrid.CommitEdit(DataGridEditingUnit.Row, exitEditingMode: true);
            _vm.ReScheduleJobsAndDrawOn(ChartCanvas);
        }
    }
}
