﻿using System;
using System.Collections.Generic;
using System.IO;

namespace McNaughton.JobSource
{
    public static class SourceLoaders
    {
        public static IEnumerable<string> ValidExtensions
            => _factory.Keys;

        public static string DefaultSourcePath
            => CsvFileSource.DefaultSourcePath;

        public static ISource Resolve(string filePath)
        {
            var extension = string.Empty;
            if (Path.HasExtension(filePath))
                extension = Path.GetExtension(
                    filePath.ToLowerInvariant());

            return _factory.TryGetValue(extension, out var maker)
                ? maker(filePath)
                : null;
        }

        private static readonly Dictionary<string, Func<string, ISource>> _factory
            = new Dictionary<string, Func<string, ISource>>
            {
                [".csv"] = path => new CsvFileSource(path)
            };
    }
}
