﻿using System.Collections.Generic;
using McNaughton.Model;

namespace McNaughton.JobSource
{
    public interface ISource
    {
        IReadOnlyCollection<IJob> Jobs();
    }
}
