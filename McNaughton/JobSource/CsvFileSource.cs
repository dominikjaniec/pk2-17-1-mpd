﻿using System.Collections.Generic;
using System.IO;
using CsvHelper;
using CsvHelper.Configuration;
using McNaughton.Model;

namespace McNaughton.JobSource
{
    public sealed class CsvFileSource : ISource
    {
        public static string DefaultSourcePath
            => Path.Combine(Directory.GetCurrentDirectory(), "data.csv");

        public string FilePath { get; }

        public CsvFileSource(string filePath = null)
            => FilePath = filePath ?? DefaultSourcePath;

        public IReadOnlyCollection<IJob> Jobs()
        {
            var config = new Configuration
            {
                Delimiter = ";",
                HasHeaderRecord = false,
                IgnoreBlankLines = true
            };

            using (var textReader = File.OpenText(FilePath))
            using (var csv = new CsvReader(textReader, config))
                return JobsEntriesFrom(csv).AsReadOnly();
        }

        private static List<IJob> JobsEntriesFrom(CsvReader csv)
        {
            var jobs = new List<IJob>();

            var id = Id.From(1);
            while (csv.Read())
            {
                var description = csv.GetField<string>(0);
                var processingTime = csv.GetField<uint>(1);

                jobs.Add(new Job(id, description, processingTime));
                id = id.Next();
            }

            return jobs;
        }
    }
}
